use number_traits::Num;

use super::zero;

#[inline]
pub fn sdiv<'a, T: Copy + Num>(out: &'a mut [T; 6], a: &[T; 6], s: T) -> &'a mut [T; 6] {
    if s == T::zero() {
        zero(out)
    } else {
        out[0] = a[0] / s;
        out[1] = a[1] / s;
        out[2] = a[2] / s;
        out[3] = a[3] / s;
        out[4] = a[4] / s;
        out[5] = a[5] / s;
        out
    }
}
#[test]
fn test_sdiv() {
    let mut v = [0, 0, 0, 0, 0, 0];
    sdiv(&mut v, &[1, 1, 1, 1, 1, 1], 1);
    assert!(v == [1, 1, 1, 1, 1, 1]);
}
